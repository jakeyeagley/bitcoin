Making local
-pip install kaggle --upgrade <-- had to be root
-Make sure kaggle.json is in .kaggle directory
-Install mysql
-Install php (sudo yum install -y httpd24 php72 mysql57-server php72-mysqlnd)
-Create database live;
-CREATE USER 'BC'@'localhost' IDENTIFIED BY 'This!!123'
-create table coinbase (PID int unsigned not null auto_increment, Timestamp int not null, Open varchar(20), High varchar(20), Low varchar(20), Close varchar(20), Volume_BTC varchar(20), Volume_Curr varchar(20), Weighted_Price varchar(20), constraint coin_prices primary key (PID) );
-create table bitstamp (PID int unsigned not null auto_increment, Timestamp int not null, Open varchar(20), High varchar(20), Low varchar(20), Close varchar(20), Volume_BTC varchar(20), Volume_Curr varchar(20), Weighted_Price varchar(20), constraint bit_prices primary key (PID) );
-GRANT ALL PRIVILEGES ON live.* TO 'BC'@'localhost';
-Navigate to bitcoin directory and run php -S localhost:8888
-Open internet browser and navigate to localhost:8888

Assumptions:
-If a row contains NaN I skipped it when importing
-Used coinbase file and used weighted_price to display price
-Security is a low priority (database password)
-Used JSON as the data interchange format

Enhancement ideas
-Have multiple lines on the charts displaying closing and opening pricing
-Improve import performance
-Fix bug with current dates
-Add logging features
-Clean up code
---Move SQL handling to a class

Design decisions
-Implemented a file import (see import.php) to run in a cron job on the server that will import the data from https://www.kaggle.com/mczielinski/bitcoin-historical-data every morning
-Used the command line program 'kaggle' to grab the data from ---Add logging features https://www.kaggle.com/mczielinski/bitcoin-historical-data
-Implemented a mysql database to hold bitcoin price data
-Used AWS to host the public facing website
-Used AWS's Linux AMI
-List of 3rd party libraries
---Chart.js graphs displaying bitcoin price data
---Bootstrap to improve UI/UX
---Jquery for imporved javascript use

Time
-3 hours Tuesday (January 14th)
-3 hours Saturday (January 18th)
