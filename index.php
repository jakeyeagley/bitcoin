<?php
echo  "<!DOCTYPE html>
       <html lang='en'>
         <head>
           <meta charset='utf-8'>
           <link rel='stylesheet' href='https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css'/>
           <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css'/>
           <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css' />
           <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js'></script>
           <script src='https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js'></script>
           <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js'></script>
           <script src='https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js'></script>
           <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js'></script>
           <script src='https://code.jquery.com/ui/1.10.4/jquery-ui.js'></script>
           <script src='/javascript/index.js'></script>
         </head>
       </html>";

echo "
<div class='container'>
  <div class='row'>
    <div class='col-md-6'>
      <label> Start Date </label>
      <input type='text' id='datepicker_start'>
    </div>
    <div class='col-md-6'>
      <label> End Date </label>
      <input type='text' id='datepicker_end'>
    </div>
  </div>
  <div class='row'>
    <div class='col-md-4'>
    </div>
    <div class='col-md-2'>
      <button id='check_button' class='btn btn-success' onclick='callGetData()'>Check</button>
    </div>
  </div>
  <div class='row'>
    <canvas id='myChart'></canvas>
  </div>
</div> ";
