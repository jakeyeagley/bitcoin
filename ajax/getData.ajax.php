<?php
$mysqli = new mysqli("localhost", "BC", "This!!123", "live");
if($mysqli->connect_error)
  die(json_encode(array('status' => 'failed', 'message' => 'Error connecting to database')));
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

$start = $_POST['start'];
$end   = $_POST['end'];

if ($start == '')
  $start = strtotime("-1 days");
else
  $start = strtotime($start);

if ($end == '')
  $end = time();
else
  $end = strtotime($end);

$stmt = $mysqli->prepare("SELECT Weighted_Price from coinbase where Timestamp>=? and Timestamp<=?");
$stmt->bind_param('ii', $start, $end);
$stmt->execute();
$result = $stmt->get_result();
$ret = array();
while($rs = $result->fetch_assoc())
{
  $ret[] = $rs['Weighted_Price'];
}
$stmt->close();
if (count($ret) > 20000)
  die(json_encode(array('status' => 'failed', 'message' => "Date range to large. Please select a smaller date range")));
else
  die(json_encode(array('status' => 'success', 'message' => $ret)));
