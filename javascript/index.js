$(document).ready(function()
{
  var myDate = new Date();
  var today     = myDate.getFullYear() - 4 + '-' + ('0'+ myDate.getMonth()+1).slice(-2) + '-' + ('0'+ myDate.getDate()).slice(-2);
  myDate = new Date(new Date().setDate(new Date().getDate()-1));
  var yesterday = myDate.getFullYear() - 4 + '-' + ('0'+ myDate.getMonth()+1).slice(-2) + '-' + ('0'+ myDate.getDate()).slice(-2);
  $(function() {$('#datepicker_start').datepicker({dateFormat: 'yy-mm-dd'})});
  $(function() {$('#datepicker_end').datepicker({dateFormat: 'yy-mm-dd'})});
  $("#datepicker_start").val(yesterday);
  $("#datepicker_end").val(today);
  callGetData();
});

$(document).on('keypress', function(e)
{
  if (e.keyCode === 13)
  {
    e.preventDefault();
    setTimeout(function() { $('#check_button').click();}, 100);
  }
})

function callGetData()
{
  let start = $('#datepicker_start').val();
  let end   = $('#datepicker_end').val();
  console.log(start);
  console.log(end);
  $.ajax({
    url: "/ajax/getData.ajax.php",
    type: "POST",
    data: { start: start, end:end },
    success: function(result)
    {
      console.log(result);
      result = JSON.parse(result);
      console.log(result);
      if (result.status != 'success')
        alert(result.message);
      else
      {
        labels = [start];
        for (let i = 0; i < 15; i++)
        {
          labels.push('');
        }
        labels.push(end);
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx,
        {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Bitcoin prices',
                    data: result.message,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                  xAxes: [{
                    scaleLabel: {
                      display: true,
                      labelString: 'Dates'
                      },
                    }],
                    yAxes: [{
                      scaleLabel: {
                        display: true,
                        labelString: 'Weighted Price'
                        },
                        ticks: {
                            beginAtZero: false
                        }
                    }]
                }
            }
        });
      }
    },
    error: function(result)
    {
      console.log('error ' + result)
    }
  });
}
