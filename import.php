<?php
$filename = 'bitcoin-historical-data.zip';
$dir      = 'data';

$output  = '';
$err_msg = '';
$wrn_msg = '';

if (file_exists($filename))
  unlink($filename);

$output .= "Reaching out for data <br> " ."\n";
exec('kaggle datasets download -d mczielinski/bitcoin-historical-data');
if (!is_dir($dir))
{
  if (!mkdir($dir))
  {
    $err_msg .= "Error in making directory data <br>" . "\n";
    exit($output . $err_msg);
  }
}
$files = scandir($dir);
foreach ($files as $f)
{
  if ($f == '.' || $f == '..')
    continue;
  if ($f == "$dir/coinbase.csv.imported" || $f == "$dir/bitstamp.csv.imported")
  {
    $key = array_search($f, $files);
    unset($files[$key]);
  }
  else
    unlink("$dir/$f");
}
exec("unzip $filename -d $dir/");

$mysqli = new mysqli("localhost", "BC", "This!!123", "live");
if($mysqli->connect_error)
{
  $err_msg .= 'Error connecting to database <br>' . "\n";
  exit($output . $err_msg);
}
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);


$files        = scandir($dir);
$stmt_coin    = $mysqli->prepare("REPLACE into `coinbase` (Timestamp, Open, High, Low, Close, Volume_BTC, Volume_Curr, Weighted_Price) values (?, ?, ?, ?, ?, ?, ?, ?)");
$stmt_bit     = $mysqli->prepare("REPLACE into `bitstamp` (Timestamp, Open, High, Low, Close, Volume_BTC, Volume_Curr, Weighted_Price) values (?, ?, ?, ?, ?, ?, ?, ?)");
$use_coin     = false;
$use_bitstamp = false;
$cnt = 0;
foreach ($files as $f)
{
  if ($f == '.' || $f == '..')
    continue;

  $fd = @fopen("$dir/$f", 'r');
  if (!$fd)
  {
    $err_msg .= "ERROR: $f not opened <br>" . "\n";
    continue;
  }

  if (substr($f, 0, 11) == 'coinbaseUSD')
  {
    $use_coin = true;
    $use_bitstamp = false;
  }
  else if (substr($f, 0, 11) == 'bitstampUSD')
  {
    $use_coin = false;
    $use_bitstamp = true;
  }
  else
  {
    $wrn_msg = "Entered into a unknown clause <br>" . "\n";
    $use_coin = false;
    $use_bitstamp = false;
  }

  $params = array();
  $types  = '';
  if ($use_coin || $use_bitstamp)
  {
    while($line = fgets($fd))
    {
      $line      = str_getcsv($line);
      $timestamp = $line[0];
      $open      = $line[1];
      $high      = $line[2];
      $low       = $line[3];
      $close     = $line[4];
      $btc       = $line[5];
      $curr      = $line[6];
      $price     = $line[7];

      if ($timestamp == 0)
        continue;
      if ($high == 'NaN' || $low == 'NaN' || $open == 'NaN' || $close == 'NaN')
        continue;

      $params[] = $timestamp;
      $params[] = $open;
      $params[] = $high;
      $params[] = $low;
      $params[] = $close;
      $params[] = $btc;
      $params[] = $curr;
      $params[] = $price;
      $types   .= 'isssssss';
      if (count($params) >= 8) // moving to more than 8 says No data supplied for parameters in prepared statement
      {
        if ($use_coin)
        {
          $stmt_coin->bind_param($types, ...$params);
          $stmt_coin->execute();
        }
        else
        {
          $stmt_bit->bind_param($types, ...$params);
          $stmt_bit->execute();
        }
        $params = array();
        $types  = '';
      }
    }
    if (count($params))
    {
        if ($use_coin)
        {
          $stmt_coin->bind_param($types, ...$params);
          $stmt_coin->execute();
        }
        else
        {
          $stmt_bit->bind_param($types, ...$params);
          $stmt_bit->execute();
        }
    }
    if ($use_coin)
      exec("mv $dir/$f $dir/coinbase.csv.imported");
    if ($use_bitstamp)
      exec("mv $dir/$f $dir/bitstamp.csv.imported");
    set_time_limit(300);
  }
}
$stmt_bit->close();
$stmt_coin->close();
$output .= "$cnt total records imported";
if ($err_msg)
  $output .= $err_msg;
if ($wrn_msg)
  $output .= $wrn_msg;

echo $output;
